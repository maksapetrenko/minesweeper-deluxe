import {gameRestartControls} from "./UI-parts/restartControls.js";
import {gameMainField} from "./UI-parts/mainField.js";
import {gameSetup} from "./actions/setup.js";


gameRestartControls.init();
gameMainField.init();
gameSetup(6);
