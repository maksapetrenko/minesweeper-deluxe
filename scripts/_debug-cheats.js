export function _showMines() {
    this.gameParameters.currentField.forEach(land => {
        if (land.hasMine) land.DOMequivalent.classList.add('exploded');
    })
}
export function _hideMines() {
    this.gameParameters.currentField.forEach(land => {
        if (land.hasMine) land.DOMequivalent.classList.remove('exploded');
    })
}

// export default function () {
//     this._showMines = _showMines;
//     this._hideMines = _hideMines;
// }