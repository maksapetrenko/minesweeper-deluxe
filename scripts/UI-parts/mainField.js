import {gameParameters as gameParams} from "../game-notions/game-params.js";
import {gameEndings} from "../actions/endings.js";

export const gameMainField = {
    DOMminefield: document.getElementById('minefield'),
        DOMplacedFlagsCounter: document.getElementById('placed-flags-counter'),
        DOMplacedMinesCounter: document.getElementById('placed-mines-counter'),

        toggleFlag(isFlaggedNow, landPiece, DOMlandPiece) {
        landPiece.isFlagged = !isFlaggedNow;
        DOMlandPiece.classList.toggle('flagged');
        if (isFlaggedNow) {
            --gameParams.placedFlags;
            DOMlandPiece.classList.add('unflagging');
            setTimeout(() => DOMlandPiece.classList.remove('unflagging'),2000)
        }
        else ++gameParams.placedFlags;
        this.DOMplacedFlagsCounter.innerText = gameParams.placedFlags.toString();
    },
    revealLandPiece(land) {
        land.isRevealed = true;
        land.DOMequivalent.classList.add('revealed');
        if (land.nearbyMinesNumber) {
            land.DOMequivalent.setAttribute('data-mines-nearby',land.nearbyMinesNumber)
        }
        gameParams.unrevealedFieldsNum -= 1;
    },

    showNearbyLand(landPiece) {
        if (landPiece.isRevealed) return //console.log(landPiece.DOMequivalent);
        const nearbyMap = landPiece.nearbyLandCoordsJSON;
        const nearbyLands = gameParams.currentField.filter(land =>
            nearbyMap.includes(JSON.stringify([land.x, land.y]))
        )

        this.revealLandPiece(landPiece);
        for (const land of nearbyLands) {
            if (land.nearbyMinesNumber > 0 && !land.isRevealed) this.revealLandPiece(land);
        }


        const landsToCheckNextCoordsJSON = JSON.stringify([
            [landPiece.x, landPiece.y - 1],
            [landPiece.x - 1, landPiece.y],
            [landPiece.x, landPiece.y + 1],
            [landPiece.x + 1, landPiece.y]
        ])
        const landsToCheckNext = gameParams.currentField.filter(land =>
            landsToCheckNextCoordsJSON.includes(JSON.stringify([land.x, land.y]))
        )
        for (const land of landsToCheckNext) {
            if (land.nearbyMinesNumber === 0 && !land.isRevealed) {
                this.showNearbyLand(land);
            }
        }

    },
    checkFieldOnClick(landPiece, DOMlandPiece) {
        if (!landPiece.isFlagged && !landPiece.isRevealed) {
            if (landPiece.hasMine) {
                return gameEndings.gameOver(DOMlandPiece);
            } else if (landPiece.nearbyMinesNumber === 0) {
                this.showNearbyLand(landPiece);
            } else {
                this.revealLandPiece(landPiece);
            }

            if (gameParams.unrevealedFieldsNum === gameParams.numOfMines) {
                gameEndings.gameVictory();
            }
        }
    },

    initFieldListeners() {

        this.DOMminefield.addEventListener('click', event => {
            if (event.target.classList.contains('landpiece')) {
                const DOMlandPiece = event.target;
                const landPiece = gameParams.currentField.find(elem => {
                    return ((+DOMlandPiece.dataset.xCoord === elem.x) && (+DOMlandPiece.dataset.yCoord === elem.y))
                })
                if (!landPiece.isFlagged) {
                    this.checkFieldOnClick(landPiece, DOMlandPiece);
                }
            }
        })

        this.DOMminefield.addEventListener('contextmenu', event => {
            event.preventDefault();
            if (event.target.classList.contains('landpiece')) {
                const DOMlandPiece = event.target;
                const landPiece = gameParams.currentField.find(elem => {
                    return ((+DOMlandPiece.dataset.xCoord === elem.x) && (+DOMlandPiece.dataset.yCoord === elem.y))
                })
                if (landPiece.isFlagged) {
                    this.toggleFlag(true, landPiece, DOMlandPiece);
                } else if (!landPiece.isFlagged && !landPiece.isRevealed) {
                    this.toggleFlag(false, landPiece, DOMlandPiece);
                }
            }
        })
    },
    init() {
        this.initFieldListeners();
    }
}