import {gameSetup} from "../actions/setup.js";

export const gameRestartControls = {
    DOMrestartButton: document.getElementById('restart-button'),
    DOMfieldSizeInput: document.getElementById('field-size'),
    DOMfieldSizeMirror: document.getElementById('field-size-mirror'),
    initInputListeners() {
        this.DOMfieldSizeInput.addEventListener('click', event => {
            event.preventDefault();
            event.stopPropagation();
        });
        this.DOMfieldSizeMirror.innerText = this.DOMfieldSizeInput.value;
        this.DOMfieldSizeInput.addEventListener('input', () => {
            this.DOMfieldSizeMirror.innerText = this.DOMfieldSizeInput.value
                ? this.DOMfieldSizeInput.value
                : '?';
        })
    },
    initButtonListener() {
        this.DOMrestartButton.addEventListener('click', event => {
            if (this.DOMfieldSizeInput.value) {
                event.preventDefault();
                gameSetup(+this.DOMfieldSizeInput.value);
            }
        })
    },
    init() {
        this.initInputListeners();
        this.initButtonListener();
    }
}