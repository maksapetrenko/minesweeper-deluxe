import {gameParameters as p} from "../game-notions/game-params.js";
import {LandPiece} from "../game-notions/LandpieceClass.js";


export function gameSetup(fieldSize) {
    const DOMfieldTable = document.querySelector('#minefield');

    const initParams = () => {
        p.setDefaultParams();
        p.numOfMines = Math.floor((fieldSize ** 2) / 6);
        p.unrevealedFieldsNum = fieldSize ** 2;
    };

    const initField = () => {
        for (let y = 1; y <= fieldSize; y++) {
            for (let x = 1; x <= fieldSize; x++) {
                p.currentField.push(new LandPiece(x, y))
            }
        }
    }

    const assignMines = () => {
        const shuffleArray = array => {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                const temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return array;
        }
        const landsToMineArr = shuffleArray([...p.currentField]);
        landsToMineArr.length = p.numOfMines;
        for (const land of landsToMineArr) {
            land.hasMine = true;
        }
    }

    const calcNearbyMinesForEach = () => {
        for (const land of p.currentField) {
            land.calcNearbyMines();
        }
    }

    const drawDOMfield = () => {
        DOMfieldTable.innerHTML = '';
        document.querySelector(':root').style.setProperty('--field-size', fieldSize);
        const field = new DocumentFragment();

        for (let y = 1; y <= fieldSize; y++) {
            for (let x = 1; x <= fieldSize; x++) {
                const DOMlandPiece = document.createElement('div');
                DOMlandPiece.classList.add('landpiece');
                DOMlandPiece.setAttribute('data-x-coord', x);
                DOMlandPiece.setAttribute('data-y-coord', y);
                field.append(DOMlandPiece);
            }
        }

        DOMfieldTable.append(field);
        document.getElementById('placed-flags-counter').innerText = p.placedFlags.toString();
        document.getElementById('placed-mines-counter').innerText = p.numOfMines.toString();
    }

    initParams();
    initField();
    assignMines();
    calcNearbyMinesForEach();
    drawDOMfield();
}
