export const gameEndings = {
    gameOver(explodedLandPiece) {
        explodedLandPiece.classList.add('exploded');
        setTimeout(() => {alert('Вы сделали неправильный выбор... Попробуйте ещё раз')},3000);
    },
    gameVictory() {
        alert('Фууух - победа!');
    }
}