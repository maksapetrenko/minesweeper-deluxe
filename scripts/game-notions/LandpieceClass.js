import {gameParameters} from "./game-params.js";

export const LandPiece = class {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.hasMine = false;
        this.isFlagged = false;
        this.isRevealed = false;
        this.nearbyMinesNumber = 0;
    }

    get nearbyLandCoordsJSON() {
        return JSON.stringify([
            [this.x - 1, this.y - 1],
            [this.x, this.y - 1],
            [this.x + 1, this.y - 1],
            [this.x - 1, this.y],
            [this.x + 1, this.y],
            [this.x - 1, this.y + 1],
            [this.x, this.y + 1],
            [this.x + 1, this.y + 1],
        ])
    };

    get DOMequivalent() {
        return document.querySelector(`div[data-x-coord="${this.x}"][data-y-coord="${this.y}"]`)
    };

    calcNearbyMines() {
        const nearbyMap = this.nearbyLandCoordsJSON;
        this.nearbyMinesNumber = gameParameters.currentField.reduce(
            (numOfMines, landPiece) => {
                if (nearbyMap.includes(JSON.stringify([landPiece.x, landPiece.y])) && landPiece.hasMine) {
                    numOfMines += 1;
                }
                return numOfMines;
            }, 0)
    }
}