export const gameParameters = {
    numOfMines: 0,
        currentField: [],
        placedFlags: 0,
        unrevealedFieldsNum: 0,
        setDefaultParams() {
        const defParams = {
            numOfMines: 0,
            currentField: [],
            placedFlags: 0,
            unrevealedFieldsNum: 0,
        }
        Object.assign(this, defParams)
    },
}