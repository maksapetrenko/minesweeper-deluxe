# [Good ol' Minesweeper: Classics Rebooted](https://maksapetrenko.gitlab.io/minesweeper-deluxe)

### As it is a never-ending-WIP-project, there is a bunch of hasty animations/unstyled "stubs" -- sorry for that.

Here is a small game created more like a training/playground than a functional game. As I find the outcome quite enjoyable, I decided to share it with the world.

It is a first pet project of mine, kind of one you discover on your account years after you made it and kills yourself with facepalm.

The intention of to create this was purely non-commercial, so sorry for not attributing assets to their authors - I just lost their credentials in the process of search.

### The stack and approaches: pure JS(ES6)&CSS3 + some graphic design.

- All the assets used were somehow redesigned from their original state, so you can witness my visual taste in action.
- On the CSS side, responsiveness was tried to be achieved with as little media queries as possible (just out of curiosity). As a result:
  - Fluid Typography via clamp() was used to make design adaptive.
  - The main field was made with CSS Grid, which rebuilds for each game using a CSS variable set from JS.
- JS has some not-that-stunning, but notable features:
  - All code is split into modules;
  - Classes are used to create logical representation of the field;
  - I has definitely tried to make the code clearer, but not always was it a success.


Thank you for attention and go enjoy a game or two: [Minesweeper](https://maksapetrenko.gitlab.io/minesweeper-deluxe).
  
